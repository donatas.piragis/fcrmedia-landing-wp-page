<?php
//Upload js and css
function fcr_files(){
    wp_enqueue_style('bootstrap', '//stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css');
    wp_enqueue_style( 'wpb-google-fonts', 'https://fonts.googleapis.com/css?family=Catamaran:100,200,300,400,500,600,700,800,900', false ); 
    wp_enqueue_style( 'wpb-google-fonts', 'https://fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i', false ); 
    wp_enqueue_style('fcr_main_styles', get_stylesheet_uri(), NULL,  microtime() );
    wp_enqueue_script('jquery', 'http://code.jquery.com/jquery-3.3.1.slim.min.js', array(), '1' , true);
    wp_enqueue_script('poper', 'http://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js', array(), '1', true);
    wp_enqueue_script('bootstrap-js', 'http://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js', array(), '1', true );
    wp_enqueue_script('main-js', get_theme_file_uri('/js/main.js'), NULL, '1.0', true);
    wp_enqueue_style('aoscss', 'https://unpkg.com/aos@2.3.1/dist/aos.css');
    wp_enqueue_script('aos-js', 'https://unpkg.com/aos@2.3.1/dist/aos.js', NULL, '1.0', true);
}
//Run js and css
add_action('wp_enqueue_scripts', 'fcr_files');

//WP menu
function page_features(){
    register_nav_menu('headerNavLocation', 'Header Menu');
    add_theme_support('title-tag');
}
//WP menu run
add_action('after_setup_theme', 'page_features');

//adding classses to navigation
function menu_li_classes($classes, $item, $args) {
    if($args->theme_location == 'headerNavLocation') {
      $classes[] = 'menu-list-items';
    }
    return $classes;
  }
  add_filter('nav_menu_css_class', 'menu_li_classes', 1, 3);

//Removing admin top bar
  function remove_admin_bar()
  {
      return false;
  }
  add_filter('show_admin_bar', 'remove_admin_bar');
  
//Custom posts
function content_posts(){
    register_post_type('contentPosts', array(
        'supports' => array('title', 'editor'),
        'public' => true,
        'labels' => array(
            'name' => 'Conent Posts',
            'add_new_item' => 'Add New Content Post',
            'edit_item' => 'Edit Content Post',
            'all_items' => 'All Content Posts',
            'singular_name' => 'Content Post'
        ),
        'menu_icon' => 'dashicons-welcome-widgets-menus' ,
    ));
}
//Custom posts run
add_action('init', 'content_posts');