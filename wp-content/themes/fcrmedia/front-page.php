<?php
get_header();
?>

<section class="hero-banner" style="background-image: url(<?php echo get_theme_file_uri('/images/testbackground.jpg')?>);">
    <div class="container">
        <div class="content-hero">
        <h1 class="heading-name">THE most amazing landing page</h1>
        <h3 class="heading-quote">Will rock your socks off</h3>
        <a href="" class="btn btn-primary btn-custom">Learn More</a>
        </div>
    </div>
</section>

<section class="content">
    <div class="container">
            <?php
            $contentPosts = new WP_Query(array(
                'posts_per_page' => 3,
                'post_type' => 'contentPosts'
            ));
            $counter = 0;
            while($contentPosts->have_posts()){
                $contentPosts->the_post();
                if($counter != 1) {
                ?>
        <div class="row align-items-center" data-aos="fade-left" data-aos-delay="300" data-aos-once="true">
            <div class="col-lg-6 order-lg-2">
                <div class="p-5">
                    <?php 
                        $image = get_field('insert_picture');
                        $url = $image['url'];
                    ?>
                <img class="img-fluid rounded-circle" src="<?php echo $url; ?>" alt="">
                </div>
             </div>
            <div class="col-lg-6 order-lg-1">
                <div class="p-5">
                    <h2 class="display-4"><?php the_title(); ?></h2>
                    <p><?php the_content(); ?></p>
                </div>
            </div>
        </div>
            <?php
                }
                else {
                    ?>
        <div class="row align-items-center" data-aos="fade-right" data-aos-delay="300" data-aos-once="true">
            <div class="col-lg-6">
                <div class="p-5">
                    <?php 
                        $image = get_field('insert_picture');
                        $url = $image['url'];
                    ?>
                    <img class="img-fluid rounded-circle" src="<?php echo $url; ?>" alt="">
                </div>
            </div>
            <div class="col-lg-6">
                <div class="p-5">
                    <h2 class="display-4"><?php the_title(); ?></h2>
                    <p><?php the_content(); ?></p>
                </div>
            </div>
        </div>
            <?php
                }
                $counter++;
                    }
            ?>
    </div>

</section>
<?php
get_footer();
?>