<footer class="site-footer">
    <div class="container">
      <p class="m-0 text-center text-white small">Copyright &copy; Your Website <?php echo date("Y"); ?></p> 
    </div>
  </footer>
  <?php wp_footer(); ?>
  <script>
  AOS.init();
</script>
</body>
</html>