<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="description" content="This is the most amazing landing page, which will rock your socks off.">
    <meta name="keywords" content="Landing, design, style, one-page">
    <?php wp_head(); ?>
</head>
<body>

<nav class="navbar navbar-expand-lg navbar-dark bg-dark header-menu-bg" id="navigation">
    <div class="container">

            <a href="<?php echo site_url() ?>" class="navbar-brand">LOGO</a>
 
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
            <?php 
                wp_nav_menu(array(
                    'theme_location' => 'headerNavLocation',
                    'container_id' => 'navbarSupportedContent',
                    'container_class'    => 'collapse navbar-collapse',
                    'menu_class' => 'nav navbar-nav ml-auto'
                ));
            ?>
    
    </div>
</nav>